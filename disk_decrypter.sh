#!/bin/bash

# TOKEN=
# CHAT_ID=
# uuid=
# sudo chmod +x var_set.sh
# ./var_set.sh

URL="https://api.telegram.org/bot$TOKEN/sendMessage"
counter=0

send_telegram () {
  curl -s -X POST $URL -d chat_id=$CHAT_ID -d text="$1"
}

while true
do
  # key is found
	if lsblk -f | grep $uuid; then
	  echo "key found, decrypting"
	  send_telegram "Device $(hostname) found key, decrypting"
	  
	  #mount device, load key
	  mount /dev/sdb1 /mnt/stc/
	  key=`cat /mnt/stc/key`
	  
	  #decrypt device with key
	  echo $key | cryptsetup luksOpen /dev/sda1 sda1 -d -
    sudo mount /dev/mapper/sda1 /mnt/sync

	  #start syncthing
	  systemctl start syncthing
	  while systemctl is-active syncthing;do
    sleep 5
    done
	  
	# no key found
  else
    echo "key not found"
	  counter=$((counter+1))
	  if test $(($counter % 1440)) -eq 1;then 
      echo "sending warning"
      send_telegram "Device $(hostname) requires key"
    fi
    sleep 5
  fi
done