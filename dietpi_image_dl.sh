# dependencies -------------------
sudo apt-get install p7zip-full -y

# getting image -------------------
mkdir download
cd download
wget "https://dietpi.com/downloads/images/DietPi_OdroidXU4-ARMv7-Buster.7z"
7z x DietPi_OdroidXU4-ARMv7-Buster.7z